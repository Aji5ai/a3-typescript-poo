# Contexte du projet
_Ce projet est fictif et réalisé dans le cadre d'un cours_

Tu vas travailler pour SimplonFactory, une entreprise innovante dans le secteur de la fabrication et de la vente en ligne.

Ta mission est de créer un système qui gère les commandes des clients, le catalogue de produits, et le processus de livraison. Ce système devra permettre de créer et de gérer des clients (Customer), de gérer un inventaire de produits (Product) avec des spécificités telles que les vêtements (Clothing) et les chaussures (Shoes), et de gérer des commandes (Order) avec une logique de livraison flexible (Deliverable).

# Initialisation du projet
```
npm init -y
npm install typescript --save-dev 
npm i --save-dev @types/node
npx tsc --init 
```
Faire un fichier index.ts dans un dossier src/

Mettre ce qui suit dans tsconfig (en remplacement de ce qui y était déjà):
```
{
  "compilerOptions": {
    "module": "nodenext",
    "moduleResolution": "nodenext",
    "target": "es2022",
    "strict": true,
    "esModuleInterop": true,
    "forceConsistentCasingInFileNames": true,
    "outDir": "./dist"
  },

  "include": ["src/**/*"],
  "exclude": ["**/*.spec.ts"]
}
```
## Pour exécuter:
Installer ts node :
```
npm install -g ts-node
```
Puis pour transpiler ET exécuter en une ligne :
```
npx ts-node src/index.ts
```