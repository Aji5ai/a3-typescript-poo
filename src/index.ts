type Dimensions = {
  length: number;
  width: number;
  height: number;
};

// Juste pour l'exemple
type Dimensions2 = [number, string, number]; // tuple avec nombre limité de valeurs (3)
type Dimensions3 = number[]; // tableau de nombres

type Address = {
  street: string;
  city: string;
  postalCode: string;
  country: string;
};

// meilleure façon de définire la class à voir avec la class Product plus bas
class Customer {
  customerId: number;
  name: string;
  email: string;
  address: Address | undefined; // address ajoutée avec une méthode plus tard donc undefined lors de la création d'instances

  constructor(customerId: number, name: string, email: string) {
    this.customerId = customerId;
    this.name = name;
    this.email = email;
  }

  setAddress(address: Address) {
    this.address = address; // address de gauche c'est la propriété de cette class, et address de droite ça sera ce qu'on va lui passer lors de la création d'une instance.
  }

  displayAddress(): string {
    return this.address
      ? `Address: ${this.address.street}, ${this.address.postalCode} ${this.address.city}, ${this.address.country}`
      : "No address found";
  }

  displayInfo(): string {
    return this.address // opérateur ternaire pour prévoir les cas où l'adresse n'existe pas encore
      ? `Customer ID: ${this.customerId}, Name: ${this.name}, Email: ${
          this.email
        }, Adresse: ${this.displayAddress()}`
      : `Customer ID: ${this.customerId}, Name: ${this.name}, Email: ${this.email}, Adresse : not specified`;
  }
}

// Version simplifiée sans redéfinir les types avant, on fait directemennt dan sle constructeur mais on précise à ce moment là la visibilité des membres : public, private ou protected
class Product {
  constructor(
    public productId: number,
    public name: string,
    public weight: number,
    public price: number,
    public dimensions: Dimensions

  ) {}
  displayDetails(): string {
    return `Product ID: ${this.productId}, Name: ${this.name}, Weight: ${this.weight}, Price: ${this.price}€, Dimensions : ${this.dimensions.length} x ${this.dimensions.width} x ${this.dimensions.height}`;
  }
}

// Vérification que les classes fonctionnent bien
const productNoSize = new Product(1, "sac", 2, 5, {
  length: 5,
  width: 5,
  height: 5,
});
//console.log(productNoSize.displayDetails());
//console.log("productNoSize", productNoSize);

let Customer1 = new Customer(1, "john", "email@email");
//console.log(Customer1.displayInfo());

// Vérification avec setAddress()
Customer1.setAddress({
  street: "2 impasse de la tuile",
  city: "montpellier",
  postalCode: "20000",
  country: "france",
});
// console.log("Customer1", Customer1.displayInfo());
// console.log("Customer1 adresse", Customer1.displayAddress());

// 3. Enums
enum ClothingSize {
  xs = "XS",
  s = "S",
  m = "M",
  l = "L",
  xl = "XL",
  xxl = "XXL",
}
// Obligation de donner des membres (les T36 etc) parce que enum assigne déjà à des chiffres sinon
enum ShoeSize { // Ils vont s'incrémenter tous de 1 à partir de 36
  T36 = 36,
  T37,
  T38,
  T39,
  T40,
  T41,
  T42,
  T43,
  T44,
  T45,
  T46,
}

// 4. Héritage
class Clothing extends Product {
  constructor(
    public productId: number,
    public name: string,
    public weight: number,
    public price: number,
    public dimensions: Dimensions,
    public size: ClothingSize
  ) {
    super(productId, name, weight, price, dimensions);
    this.size = size;
  }

  // hérite automatiquement des méthodes de Product
  // on modifie une des méthodes donc on doit la ré écrire :
  displayDetails(): string {
    return `Product ID: ${this.productId}, Name: ${this.name}, Weight: ${this.weight}, Price: ${this.price}€, Dimensions : ${this.dimensions.length} x ${this.dimensions.width} x ${this.dimensions.height}, Size : ${this.size}`;
  }
}

const pantalon = new Clothing(
  1,
  "pantalon",
  0.4,
  25,
  {length: 1, width: 0.2, height: 1},
  ClothingSize.m //les enum de strings doivent être appelés comme ça
);
//console.log(pantalon.displayDetails());

class Shoes extends Product {
  constructor(
    public productId: number,
    public name: string,
    public weight: number,
    public price: number,
    public dimensions: Dimensions,
    public size: ShoeSize
  ) {
    super(productId, name, weight, price, dimensions);
    this.size = size;
  }

  // hérite automatiquement des méthodes de Product
  // on modifie une des méthodes donc on doit la ré écrire :
  displayDetails(): string {
    return `Product ID: ${this.productId}, Name: ${this.name}, Weight: ${this.weight}, Price: ${this.price}€, Dimensions : ${this.dimensions.length} x ${this.dimensions.width} x ${this.dimensions.height}, Size : ${this.size}`;
  }
}

const talons = new Shoes(
  2,
  "talons",
  0.7,
  56,
  {length: 0.45, width: 0.2, height: 0.6},
  40 //les enum de chiffres peuvent être appelés comme ça, ou avec ShoeSize.T40
);
//console.log(talons.displayDetails());

// 5. Méthodes
class Order {
  delivery: Deliverable | undefined; // 5. On ne devait pas modifier le constructeur

  constructor(
    public orderId: number,
    public customer: Customer,
    public productsList: Product[], // tableau dans lequel y'a des produits. Les produits sont sous forme d'objets donc c'est un tableau d'objets
    public orderDate: string
  ) {}

  // Ajoute un produit dans le tableau de produits (prosductsList). Le produit doit être un objet déja créé comme pantalon
  addProduct(product: Product): void {
    // void car ne retourne rien de spécial, réalise juste une action
    this.productsList.push(product);
  }

  // Retire un produit par son id
  removeProduct(productId: number): void {
    this.productsList = this.productsList.filter(
      (item: Product) => item.productId !== productId
    );
  }

  // Calcule le poids total de la commande
  calculateWeight(): number {
    let weightOrder: number = 0;
    for (const item of this.productsList) {
      // for of conseillé pour itérables (tableaux, listes) et for in pour objets
      weightOrder += item.weight;
    }
    return weightOrder;
  }

  // Calcule le prix total de la commande
  calculateTotal(): number {
    let priceOrder: number = 0;
    for (const item of this.productsList) {
      priceOrder += item.price;
    }
    return priceOrder;
  }

  // Affiche les détails de la commande : les informations de l'utilisateur, les informations de chaque produit et le total de la commande.
  displayOrder(): string {
    // Pour récupérer les infos de chaque objet commandé
    let productsInfo: string = "";
    for (const item of this.productsList) {
      productsInfo += `${item.displayDetails()}\n`;
    }

    // pas besoin de reprendre customer.name .address etc, tout est déjà dans displayInfo
    return `Informations de l'utilisateur: 
    \n ${this.customer.displayInfo()}
    \n Produits commandés : 
    \n ${productsInfo}
    \n Prix total de la commande : ${this.calculateTotal()}€`;
  }


  // ajouts de la partie 6 interfaces du brief (cf interface Deliverable créée tout en bas du script)
  setDelivery(delivery: Deliverable) {
    this.delivery = delivery; // permet d'assigner le type de livraison le nom de la class StandardDelivery ou ExpressDelivery qui sont donc de type/interface Deliverable
  }

  // Utilise la méthode calculateShippingFee de l'objet delivery pour calculer les frais de livraison en fonction du poids total de la commande.
  calculateShippingCosts() {
    return this.delivery?.calculateShippingFee(this.calculateWeight());
  }

  // Utilise la méthode estimateDeliveryTime de l'objet delivery pour estimer le délai de livraison de la commande.
  estimateDeliveryTime() {
    return this.delivery?.estimateDeliveryTime(this.calculateWeight());
  }
}

// Vérification pour add Product :
const newOrder = new Order(42, Customer1, [talons], "02/02/24"); // on doit lui passer des customer et product déjà existants, des instances de ces classes. Il faut donc créer des instances avant pour pouvoir les passer à order.
// console.log("newOrder.productsList", newOrder.productsList);
newOrder.addProduct(pantalon);
// console.log("newOrder.productsList updated", newOrder.productsList);

// Vérification pour removeProduct :
newOrder.removeProduct(2); // enleve talons qui a un productId de 2
//console.log("talons enlevés", newOrder.productsList);

// Vérification pour calculate Weight :
//console.log(`Poids total : ${newOrder.calculateWeight()}kg`);

// Vérification pour calculate Total :
//console.log(`Prix total : ${newOrder.calculateTotal()}€`);

// Vérification pour display Order :
//console.log(newOrder.displayOrder());

// 6. Interfaces (complète la class Order)
interface Deliverable {
  // Estime le délai de livraison en nombre de jours.
  estimateDeliveryTime(weight: number): number;

  // Calcule les frais de livraison.
  calculateShippingFee(weight: number): number;
}

class StandardDelivery implements Deliverable {
  estimateDeliveryTime(weight: number): number {
    return weight<10 ? 7 : 10;
  }
  calculateShippingFee(weight: number): number {
    if (weight<1){
      return 5;
    } else if (weight<=5){
      return 10;
    } else{
      return 20;
    };
  }
}

class ExpressDelivery implements Deliverable {
  estimateDeliveryTime(weight: number): number {
    return weight < 5 ? 1 : 3;
  }
  calculateShippingFee(weight: number): number {
    if (weight < 1) {
      return 8;
    } else if (weight <= 5) {
      return 14;
    } else {
      return 30;
    }
  }
}

// Création d'instances de Standard Deliver et Express Delivery
const livraison1 = new StandardDelivery();
const livraison2 = new ExpressDelivery;

// Vérifications des ajouts du 6/
newOrder.setDelivery(livraison2); // Attention à ne pas appeler sur Standard Delivery ou Express mais bien sur une de leurs instances.

// Important de spécifier un setDelivery sinon ça retournera undefined en appelant les console log suivants :
// console.log("Coûts de livraison estimés :", newOrder.calculateShippingCosts())
// console.log("Temps de livraison en jours :", newOrder.estimateDeliveryTime())

